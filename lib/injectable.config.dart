// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import 'flows/map/cubit/map_cubit.dart' as _i5;
import 'flows/map/repository/map_repository.dart' as _i3;
import 'flows/map/repository/map_repository_impl.dart'
    as _i4; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.factory<_i3.MapRepository>(() => _i4.MapRepositoryImpl());
  gh.factory<_i5.MapCubit>(() => _i5.MapCubit(get<_i3.MapRepository>()));
  return get;
}
