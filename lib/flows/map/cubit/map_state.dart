part of 'map_cubit.dart';

@immutable
abstract class MapState {}

class MapInitial extends MapState {}

class MapWayPointFetched extends MapState {
  final List<WayPoint> wayPoints;

  MapWayPointFetched(this.wayPoints);
}

class MapFetchedJson extends MapState {
  final GoogleMapController googleMapController;
  final Set<Polyline>? polylines;
  final Set<Marker>? markers;
  MapFetchedJson(
    this.googleMapController, {
    this.polylines,
    this.markers,
  });
}
