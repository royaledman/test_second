import 'dart:async';
import 'dart:math';
import 'dart:typed_data';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:test_second/data/waypoint.dart';
import 'package:test_second/flows/map/repository/map_repository.dart';
import 'dart:ui' as ui;
part 'map_state.dart';

@injectable
class MapCubit extends Cubit<MapState> {
  final MapRepository mapRepository;
  MapCubit(this.mapRepository) : super(MapInitial());
  WayPoint? selectedWayPoint;

  List<LatLng>? wholeLine;

  chooseWayPoint(WayPoint wayPoint) {
    if (selectedWayPoint != wayPoint) {
      selectedWayPoint = wayPoint;
    } else {
      selectedWayPoint = null;
    }

    if (state is MapWayPointFetched) {
      emit(MapWayPointFetched((state as MapWayPointFetched).wayPoints));
    }
  }

  getWayPoints() async {
    final wayPoints = await mapRepository.getWayPoints();

    if (wayPoints != null) emit(MapWayPointFetched(wayPoints));
  }

  getJsonFile(GoogleMapController googleMapController) async {
    final data = await mapRepository.fetchJsonMapFile();

    await setStyle(googleMapController);

    wholeLine = data!['points']
        .map((e) => LatLng(e['lat'], e['lng']))
        .toList()
        .cast<LatLng>();

    constructPassedPolyline(googleMapController, data);
  }

  constructPassedPolyline(GoogleMapController googleMapController, data) async {
    Set<Polyline> polylines = state is MapFetchedJson
        ? (state as MapFetchedJson).polylines ?? {}
        : {};

    Set<Marker> markers =
        state is MapFetchedJson ? (state as MapFetchedJson).markers ?? {} : {};

    polylines.add(Polyline(
        polylineId: const PolylineId('other_way'),
        color: Colors.grey,
        width: 5,
        points: wholeLine!));
    polylines.add(Polyline(
        polylineId: const PolylineId('passed_way'),
        color: Colors.green,
        width: 5,
        points: data['points']
            .takeWhile((element) =>
                element['lng'] != selectedWayPoint!.latLng.longitude &&
                element['lat'] != selectedWayPoint!.latLng.latitude)
            .toList()
            .map((e) => LatLng(e['lat'], e['lng']))
            .toList()
            .cast<LatLng>()));

    markers.addAll(await constructMarkers(data));

    emit(MapFetchedJson(googleMapController,
        polylines: polylines, markers: markers));
  }

  constructMarkers(data) async {
    return {
      Marker(
          markerId: MarkerId('car'),
          icon: BitmapDescriptor.fromBytes(
              await getBytesFromAsset('assets/red-car.png', 120)),
          position: selectedWayPoint!.latLng),
      Marker(
          markerId: MarkerId('start'),
          icon: BitmapDescriptor.fromBytes(
            await getBytesFromAsset('assets/ellipse.png', 120),
          ),
          position:
              LatLng(data['points'].first['lat'], data['points'].first['lng'])),
      Marker(
          markerId: MarkerId('dest'),
          icon: BitmapDescriptor.fromBytes(
            await getBytesFromAsset('assets/ellipse.png', 120),
          ),
          position:
              LatLng(data['points'].last['lat'], data['points'].last['lng'])),
    };
  }

  setStyle(GoogleMapController googleMapController) async {
    final styles = await mapRepository.getMapStyle();

    googleMapController.setMapStyle(styles);
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }

  Timer? timer;

  bool playing = false;

  playTrack(BuildContext context) async {
    Set<Polyline> polylines = state is MapFetchedJson
        ? (state as MapFetchedJson).polylines ?? {}
        : {};

    Set<Marker> markers =
        state is MapFetchedJson ? (state as MapFetchedJson).markers ?? {} : {};

    if (state is MapFetchedJson) {
      playing = true;
      timer = Timer.periodic(const Duration(milliseconds: 100), (timer) async {
        Polyline currentPolyline = (state as MapFetchedJson)
            .polylines!
            .where((element) =>
                element.polylineId == const PolylineId('passed_way'))
            .first;

        if (currentPolyline.points.length < wholeLine!.length) {
          (state as MapFetchedJson)
              .markers!
              .removeWhere((element) => element.markerId == MarkerId('car'));
          LatLng currentPoint = currentPolyline.points.isEmpty
              ? wholeLine!.first
              : currentPolyline.points.last;
          LatLng nextPoint = wholeLine![currentPolyline.points.length];

          markers.add(Marker(
              markerId: const MarkerId('car'),
              anchor: const Offset(0.2, 0.5),
              rotation: atan2(nextPoint.longitude - currentPoint.longitude,
                      nextPoint.latitude - nextPoint.latitude) *
                  90 /
                  pi,
              alpha: 1.0,
              icon: BitmapDescriptor.fromBytes(
                  await getBytesFromAsset('assets/red-car.png', 120)),
              position: wholeLine![currentPolyline.points.length]));

          (state as MapFetchedJson)
              .googleMapController
              .animateCamera(CameraUpdate.newCameraPosition(
                CameraPosition(
                    target: wholeLine![currentPolyline.points.length],
                    zoom: 14.5),
              ));

          currentPolyline.points.add(wholeLine![currentPolyline.points.length]);
          polylines.add(currentPolyline);

          emit(MapFetchedJson((state as MapFetchedJson).googleMapController,
              polylines: polylines, markers: markers));
        } else {
          timer.cancel();
          ScaffoldMessenger.of(context)
              .showSnackBar(const SnackBar(content: Text('We Arrived!')));
        }
      });
    }
  }

  stopTrack() {
    Set<Marker> markers =
        state is MapFetchedJson ? (state as MapFetchedJson).markers ?? {} : {};
    Set<Polyline> polylines = state is MapFetchedJson
        ? (state as MapFetchedJson).polylines ?? {}
        : {};
    if (timer != null) {
      playing = false;
      timer!.cancel();
      emit(MapFetchedJson((state as MapFetchedJson).googleMapController,
          polylines: polylines, markers: markers));
    }
  }
}
