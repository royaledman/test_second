import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:test_second/data/waypoint.dart';
import 'package:test_second/flows/map/cubit/map_cubit.dart';

class MapIndex extends StatelessWidget {
  final WayPoint wayPoint;
  const MapIndex({Key? key, required this.wayPoint}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = BlocProvider.of<MapCubit>(context);
    return BlocBuilder<MapCubit, MapState>(
      builder: (context, state) {
        return Scaffold(
            floatingActionButton: state is MapFetchedJson
                ? FloatingActionButton(
                    onPressed: () => !cubit.playing
                        ? cubit.playTrack(context)
                        : cubit.stopTrack(),
                    child: Icon(cubit.playing ? Icons.stop : Icons.play_arrow))
                : null,
            body: Stack(
              children: [
                GoogleMap(
                  myLocationButtonEnabled: false,
                  polylines: state is MapFetchedJson ? state.polylines! : {},
                  markers: state is MapFetchedJson ? state.markers! : {},
                  initialCameraPosition: CameraPosition(
                    target: wayPoint.latLng,
                    zoom: 14.4746,
                  ),
                  onMapCreated: (GoogleMapController controller) {
                    BlocProvider.of<MapCubit>(context).getJsonFile(controller);
                  },
                ),
              ],
            ));
      },
    );
  }
}
