import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:test_second/flows/map/cubit/map_cubit.dart';

class WayPointSelectPage extends StatelessWidget {
  const WayPointSelectPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cubit = BlocProvider.of<MapCubit>(context);
    cubit.getWayPoints();
    return BlocBuilder<MapCubit, MapState>(
      builder: (context, state) {
        return Scaffold(
            floatingActionButton: cubit.selectedWayPoint != null
                ? FloatingActionButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/map',
                              arguments: cubit.selectedWayPoint)
                          .then((value) => cubit.getWayPoints());
                    },
                    child: const Icon(Icons.arrow_forward_ios),
                  )
                : null,
            body: state is MapWayPointFetched
                ? ListView.builder(
                    itemCount: state.wayPoints.length,
                    itemBuilder: (context, index) => CheckboxListTile(
                        title: Html(
                          data: state.wayPoints[index].dir,
                        ),
                        subtitle: Text(
                            'Distance : ${state.wayPoints[index].dist} . Time : ${state.wayPoints[index].timeTo}'),
                        value: cubit.selectedWayPoint == state.wayPoints[index],
                        onChanged: (value) {
                          cubit.chooseWayPoint(state.wayPoints[index]);
                        }))
                : const Center(child: CircularProgressIndicator()));
      },
    );
  }
}
