import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:injectable/injectable.dart';
import 'package:test_second/data/waypoint.dart';
import 'package:test_second/flows/map/repository/map_repository.dart';

@Injectable(as: MapRepository)
class MapRepositoryImpl implements MapRepository {
  @override
  Future<Map<String, dynamic>?> fetchJsonMapFile() async {
    final jsonFile = await rootBundle.loadString('assets/route_json.json');

    return jsonDecode(jsonFile);
  }

  @override
  Future<String> getMapStyle() async {
    final jsonFile = await rootBundle.loadString('assets/map_style.json');

    return jsonFile;
  }

  @override
  Future<List<WayPoint>?> getWayPoints() async {
    final data = await fetchJsonMapFile();

    if (data != null)
      return data['points']
          .expand((e) => [
                if (e['type'] == 'waypoint' &&
                    e['dist'] != null &&
                    e['timeto'] != null)
                  e
              ])
          .map((e) => WayPoint.fromJson(e))
          .toList()
          .cast<WayPoint>();
  }
}
