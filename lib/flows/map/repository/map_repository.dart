import 'package:test_second/data/waypoint.dart';

abstract class MapRepository {
  Future<Map<String, dynamic>?> fetchJsonMapFile() async {}

  Future<String?> getMapStyle() async {}

  Future<List<WayPoint>?> getWayPoints() async {}
}
