import 'package:google_maps_flutter/google_maps_flutter.dart';

class WayPoint {
  LatLng latLng;
  String dist;
  String timeTo;
  String dir;

  WayPoint.fromJson(Map json)
      : latLng = LatLng(json['lat'], json['lng']),
        dist = json['dist']['txt'],
        timeTo = json['timeto']['txt'],
        dir = json['dir'];
}
