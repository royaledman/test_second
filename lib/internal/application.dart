import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_second/flows/map/cubit/map_cubit.dart';
import 'package:test_second/injectable.dart';
import 'package:test_second/internal/router.dart';

class MyApp extends StatelessWidget {
  final AppRouter appRouter;
  const MyApp({Key? key, required this.appRouter}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt.get<MapCubit>(),
      child: MaterialApp(
        onGenerateRoute: appRouter.generateRouter,
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
