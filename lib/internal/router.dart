import 'package:flutter/material.dart';
import 'package:test_second/data/waypoint.dart';
import 'package:test_second/flows/map/screen/create.dart';
import 'package:test_second/flows/map/screen/index.dart';

class AppRouter {
  Route generateRouter(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (context) => WayPointSelectPage());
      default:
        return MaterialPageRoute(
            builder: (context) => MapIndex(
                  wayPoint: settings.arguments as WayPoint,
                ));
    }
  }
}
