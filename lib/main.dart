import 'package:flutter/material.dart';
import 'package:test_second/injectable.dart';
import 'package:test_second/internal/router.dart';

import 'internal/application.dart';

void main() {
  configureDependencies();
  runApp(MyApp(
    appRouter: AppRouter(),
  ));
}
